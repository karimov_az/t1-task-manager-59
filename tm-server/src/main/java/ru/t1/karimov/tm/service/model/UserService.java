package ru.t1.karimov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.model.IUserRepository;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.model.IProjectService;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.api.service.model.ITaskService;
import ru.t1.karimov.tm.api.service.model.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.exception.field.EmailEmptyException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.ExistsEmailException;
import ru.t1.karimov.tm.exception.user.ExistsLoginException;
import ru.t1.karimov.tm.exception.user.RoleEmptyException;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.model.UserRepository;
import ru.t1.karimov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private final IProjectService projectService;

    @NotNull
    @Autowired
    private final ITaskService taskService;

    @NotNull
    @Autowired
    private final ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        @NotNull final String userId = model.getId();
        if (!existsById(userId)) throw new UserNotFoundException();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        sessionService.removeAll(userId);
        repository.removeOne(model);
    }

    @Override
    @Transactional
    public void removeAll() throws Exception {
        taskService.removeAll();
        projectService.removeAll();
        sessionService.removeAll();
        repository.removeAll();
    }

    @Override
    @Transactional
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        removeOne(
                Optional.ofNullable(findByLogin(login))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    @Transactional
    public void removeOneByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        removeOne(
                Optional.ofNullable(findByEmail(email))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findOneById(id))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final String fName = (firstName == null) ? "" : firstName;
        @NotNull final String lName = (lastName == null) ? "" : lastName;
        @NotNull final String mName = (middleName == null) ? "" : middleName;
        @NotNull final User user = Optional.ofNullable(repository.findOneById(id))
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(fName);
        user.setLastName(lName);
        user.setMiddleName(mName);
        repository.update(user);
        return user;
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        repository.update(user);
    }

}
