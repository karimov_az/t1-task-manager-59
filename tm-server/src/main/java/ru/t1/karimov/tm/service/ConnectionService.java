package ru.t1.karimov.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

@Service
@AllArgsConstructor
public class ConnectionService implements IConnectionService {

    @NotNull
    private static final String CHANGELOG_XML = "changelog/changelog-master.xml";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @SneakyThrows
    private Connection getConnection() {
        return DriverManager.getConnection(
                propertyService.getDatabaseUrl(),
                propertyService.getDatabaseUsername(),
                propertyService.getDatabasePassword()
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public Liquibase getLiquibase() {
        @NotNull final Connection connection = getConnection();
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase(CHANGELOG_XML, accessor, database);
    }

}
