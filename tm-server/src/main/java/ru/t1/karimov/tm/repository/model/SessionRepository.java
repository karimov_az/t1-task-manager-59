package ru.t1.karimov.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.model.ISessionRepository;
import ru.t1.karimov.tm.model.Session;

import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(s) FROM Session s";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final String jpql = "SELECT s FROM Session s";
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final Comparator<Session> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT s FROM Session s ORDER BY s." + sort;
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT s FROM Session s";
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT s FROM Session s WHERE s.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId, @NotNull final Comparator<Session> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT s FROM Session s WHERE s.user.id = :userId ORDER BY s." + sort;
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT s FROM Session s WHERE s.id = :id AND s.user.id = :userId";
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Session findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT s FROM Session s WHERE s.user.id = :userId";
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class)
                .setParameter(USER_ID, userId)
                .setFirstResult(index);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(s) FROM Session s WHERE s.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

}
