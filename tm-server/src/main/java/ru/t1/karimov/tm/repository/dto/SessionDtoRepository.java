package ru.t1.karimov.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.karimov.tm.dto.model.SessionDto;

import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(s) FROM Session s";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll() throws Exception {
        @NotNull final String jpql = "SELECT s FROM SessionDto s";
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll(@NotNull final Comparator<SessionDto> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT s FROM SessionDto s ORDER BY s." + sort;
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public SessionDto findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(SessionDto.class, id);
    }

    @Nullable
    @Override
    public SessionDto findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT s FROM SessionDto s";
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId";
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll(@NotNull final String userId, @NotNull final Comparator<SessionDto> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId ORDER BY s." + sort;
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public SessionDto findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId AND s.id = :id";
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public SessionDto findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId";
        @NotNull final TypedQuery<SessionDto> query = entityManager.createQuery(jpql, SessionDto.class)
                .setParameter(USER_ID, userId)
                .setFirstResult(index);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(s) FROM SessionDto s WHERE s.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String jpql = "DELETE FROM SessionDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM SessionDto s WHERE s.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter(USER_ID, userId)
                .executeUpdate();
    }

}
