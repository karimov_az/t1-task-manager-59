package ru.t1.karimov.tm.endpoint;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.dto.IUserDtoService;
import ru.t1.karimov.tm.dto.model.SessionDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.karimov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLoginRequest request
    ) throws Exception {
        try {
            @NotNull final IAuthService authService = getServiceLocator().getAuthService();
            @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLogoutRequest request
    ) throws Exception {
        try {
            @NotNull final SessionDto session = check(request);
            @NotNull final IAuthService authService = getServiceLocator().getAuthService();
            authService.invalidate(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserProfileRequest request
    ) throws Exception {
        try {
            @NotNull final SessionDto session = check(request);
            @Nullable final String userId = session.getUserId();
            @NotNull final IUserDtoService userService = getServiceLocator().getUserService();
            @Nullable final UserDto user = userService.findOneById(userId);
            return new UserProfileResponse(user);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

}
