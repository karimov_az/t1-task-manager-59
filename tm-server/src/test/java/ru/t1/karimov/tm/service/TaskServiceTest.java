package ru.t1.karimov.tm.service;

import org.junit.FixMethodOrder;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskServiceTest extends AbstractSchemeTest {
/*

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @After
    public void clean() throws Exception {
        userService.removeOneByLogin("user");
        userService.removeOneByLogin("admin");
    }

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initTest() throws Exception {
        @NotNull final ISessionService sessionService = new SessionService(connectionService);
        @NotNull final IProjectService projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        userService = new UserService(connectionService, propertyService, projectService, taskService, sessionService);
        taskList = new ArrayList<>();

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.salt(propertyService, "admin"));
        admin.setEmail("admin@testAdmin.ru");
        admin.setLastName("admin");
        admin.setFirstName("admin");
        admin.setMiddleName("admin");
        admin.setRole(Role.ADMIN);
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.salt(propertyService, "user"));
        user.setEmail("user@testUser.ru");
        user.setLastName("user");
        user.setFirstName("user");
        user.setMiddleName("user");

        userService.add(admin);
        userService.add(user);
        taskList.add(new Task(admin,"Task 1", "Admin task 1", Status.IN_PROGRESS));
        taskList.add(new Task(admin, "Task 2", "Admin task 2", Status.NOT_STARTED));
        taskList.add(new Task(user, "Task 1", "User task 1", Status.NOT_STARTED));
        taskList.add(new Task(user, "Task 2", "User task 2", Status.NOT_STARTED));
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @Test
    public void testAdd() throws Exception {
        @Nullable final User user = userService.findByLogin("user");
        assertNotNull(user);
        @NotNull final String userId = user.getId();
        assertEquals(4, taskService.getSize().intValue());
        @NotNull final Task task = new Task(user,"Task 1", "Admin task 1", Status.IN_PROGRESS);
        taskService.add(userId, task);
        assertEquals(5, taskService.getSize().intValue());
    }

    @Test
    public void testAddAll() throws Exception {
        @NotNull List<Task> taskList = new ArrayList<>();
        @Nullable final User user = userService.findByLogin("user");
        assertNotNull(user);
        for (int i = 0; i < 10; i++) {
            @NotNull final Task task = new Task(user, "Task 1" + i, "User", Status.NOT_STARTED);
            taskList.add(task);
        }
        taskService.add(taskList);
        assertEquals(14, taskService.getSize().intValue());
    }

    @Test
    public void testClearAdmin() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("user").getId();
        taskService.removeAll(adminUserId);
        assertEquals(0, taskService.getSize(adminUserId).intValue());
        assertEquals(0, taskService.getSize(userId).intValue());
    }

    @Test
    public void testClearUser() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        taskService.removeAll(userId);
        assertEquals(0, taskService.getSize(userId).intValue());
    }

    @Test
    public void testFindAllAdmin() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Task> tasks = taskService.findAll(userId);
        assertEquals(taskService.getSize(userId).intValue(), tasks.size());
    }

    @Test
    public void testFindAllUser() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull List<Task> taskList = taskService.findAll(userId);
        assertEquals(2, taskList.size());
    }

    @Test
    public void testFindByIdAdmin() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        assertNotNull(taskService.findOneById(userId, taskId));
    }

    @Test
    public void testFindByIdUser() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        assertNull(taskService.findOneById(userId, taskId));
    }

    @Test
    public void testGetSizeAdmin() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        assertEquals(2, taskService.getSize(userId).intValue());
    }

    @Test
    public void testGetSizeUser() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        assertEquals(2, taskService.getSize(userId).intValue());
    }

    @Test
    public void testRemoveByIdUser() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.removeOneById(userId, taskId);
    }

    @Test
    public void testRemovedByIdAdmin() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.removeOneById(userId, taskId);
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        assertNotNull(taskService.updateTaskById(userId, taskId, "new name", "new description"));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdEmptyId() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "";
        taskService.updateTaskById(userId, taskId, "new name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdEmptyName() throws Exception {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.updateTaskById(userId, taskId, "", "new description");
    }
*/

}
