package ru.t1.karimov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.response.AbstractProjectResponse;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final ProjectDto project) {
        super(project);
    }

}
