package ru.t1.karimov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.domain.DataYamlSaveFasterXmlRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String DESCRIPTION = "Save data to yaml file.";

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE YAML]");
        domainEndpoint.saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

}
