package ru.t1.karimov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserProfileResponse response = authEndpoint.profile(request);
        @Nullable final UserDto user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}
