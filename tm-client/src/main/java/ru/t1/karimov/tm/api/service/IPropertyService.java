package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
